FROM php:7.2-apache
LABEL Name=cmparrela/cmparrela Version=1.0.0

# Instala o basico
RUN apt-get update && apt-get install -y vim git locales wget git 

# Instala o cURL
RUN apt-get install -y libcurl4-openssl-dev
RUN docker-php-ext-install curl

# Instala algumas extensões comum
RUN apt-get install -y zlib1g-dev zip 
RUN docker-php-ext-install mysqli pdo_mysql iconv zip exif mbstring

# Seta a localização pt_BR
RUN echo "pt_BR.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen
ENV LANG pt_BR.UTF-8
ENV LC_ALL pt_BR.UTF-8

# Funcionamento do composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
CMD composer install

# Roda os comandos do artisan
CMD php artisan migrate:install
CMD php artisan migrate
CMD php artisan key:generate
CMD php artisan serve --host=0.0.0.0 --port=8000