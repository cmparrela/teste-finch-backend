<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'tarefa'], function () {
    Route::get('/', ['uses' => 'TarefaController@lista']);
    Route::get('/{id}', ['uses' => 'TarefaController@mostra']);
    Route::post('/', ['uses' => 'TarefaController@cria']);
    Route::put('/{id}', ['uses' => 'TarefaController@edita']);
    Route::delete('/{id}', ['uses' => 'TarefaController@deleta']);
});