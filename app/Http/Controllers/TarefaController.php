<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Tarefa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Exception;

class TarefaController extends Controller
{
    private $tarefa;
    
    public function __construct(Tarefa $tarefa)
    {
        $this->tarefa = $tarefa;
    }
    
    /**
    * Lista todas as tarefas
    *
    * @return void
    */
    public function lista()
    {
        $tarefas = $this->tarefa->all();
        return response()->json(['status' => true, 'retorno' => $tarefas]);
    }
    
    /**
    * Mostra somente uma tarefa
    *
    * @param [type] $codigo
    * @return void
    */
    public function mostra($codigo)
    {
        $tarefas = $this->tarefa->find($codigo);
        return response()->json(['status' => true, 'retorno' => $tarefas]);
    }
    
    /**
    * Cria uma tarefa
    *
    * @param Request $request
    * @return void
    */
    public function cria(Request $request)
    {
        $validacao = $this->validacao($request->all());
        if (!empty($validacao)) {
            return response()->json(['status' => false, 'mensagem' => $validacao]);
        }
        
        $dados = [
            'titulo' => $request->input('titulo'),
            'descricao' => $request->input('descricao'),
            'situacao' => $request->input('situacao'),
        ];
        
        $retorno = $this->tarefa->create($dados);
        
        return response()->json(['status' => true, 'retorno' => $retorno->toArray()]);
    }
    
    /**
    * Edita a tarefa
    *
    * @param [type] $codigo
    * @param Request $request
    * @return void
    */
    public function edita($codigo, Request $request)
    {
        $validacao = $this->validacao($request->all());
        if (!empty($validacao)) {
            return response()->json(['status' => false, 'mensagem' => $validacao]);
        }
        
        $tarefa = $this->tarefa->find($codigo); 
        if (!$tarefa) {
            return response()->json(['status' => false, 'mensagem' => 'Tarefa não encontrada']);
        }
        
        $tarefa->titulo = $request->input('titulo');
        $tarefa->descricao = $request->input('descricao');
        $tarefa->situacao = $request->input('situacao');
        $tarefa->save();
        
        return response()->json(['status' => true, 'retorno' => $tarefa->toArray()]);
    }
    
    /**
    * Deleta permanentemente o registro
    *
    * @param [type] $codigo
    * @return void
    */
    public function deleta($codigo){
        $retorno = $this->tarefa->find($codigo)->delete();
        return response()->json(['status' => $retorno]);
    }
    
    /**
    * Valida se os campos obrigatorios estão preenchidos
    *
    * @param [type] $dados
    * @return void
    */
    private function validacao($dados){
        $erros = [];
        
        $validacao = Validator::make($dados, [
            'titulo' => 'required|string',
            'situacao' => 'required|integer',
            ]);
            
            // Extrai somente as mensagens de erro
            if ($validacao->fails()) {
                foreach ($validacao->errors()->all() as $erro) {
                    $erros[] = $erro;
                }
            }
            
            return $erros;
        }
    }
    