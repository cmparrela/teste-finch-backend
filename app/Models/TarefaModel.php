<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tarefa extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'codigo';
    protected $table = 'tarefa';
    protected $fillable = ['titulo', 'descricao', 'situacao'];
    protected $guard = ['codigo'];
    
}
