
##Instalação do docker

Primeiro passo é instalar o docker e o docker compose, seguindo a documentação oficial
https://docs.docker.com/install/linux/docker-ce/ubuntu/
https://docs.docker.com/compose/install/


##Buildando
Baixar repositório. Dentro da pasta baixada rodar o comando para subir o docker
```sh
docker-compose up -d
```

Caso seja necessário buildar novamente rode o comando
```sh
docker-compose up -d --build
```

O nome dos nossos containers ficaram `finch_web_1` e `finch_db_1` caso o nome da pasta não tenha sido alterada, o restante das instruções vai seguir considerando que esses foram usado nomes.


##Instruções
- Criar um banco de dados que será utilizado na aplicação

- Replicar o `env.example` para `env` e colocar o nome do banco de dados criado em `DB_DATABASE` e o nome do container em `DB_HOST`

- Acessar o container com o comando
```sh
docker exec -it finch_web_1 bash 
```
- Dentro do container rodar os comandos na sequencia

```sh
composer install
php artisan key:generate
php artisan migrate:install
php artisan migrate
php artisan serve --host=0.0.0.0 --port=8000
```

- Se tudo der certo a aplicação estara rodando em `http://localhost:8000`


